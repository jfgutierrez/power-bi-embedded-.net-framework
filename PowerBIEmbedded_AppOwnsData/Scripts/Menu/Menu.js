﻿/*jQuery time*/
$(document).ready(function () {
	(function ($) {
		var Menu = {
			init: function () {
				this.cache();
				this.bindEnvets();
			},
			cache: function () {
				this.menuAccordian = $("#menuAccordian h3");
				this.btnEmbedReport = $("#btnEmbedReport");
				this.embeddeReport = $("#embeddeReport");
			},

			bindEnvets: function () {
				var self = Menu;
				self.menuAccordian.click(this.ShowMenu);
				self.btnEmbedReport.click(this.LoadReport);
			},
			ShowMenu: function () {
				//slide up all the link lists
				$("#menuAccordian ul ul").slideUp();
				//slide down the link list below the h3 clicked - only if its closed
				if (!$(this).next().is(":visible")) {
					$(this).next().slideDown();
				}
			},
			LoadReport: function () {
				var self = Menu;
				self.embeddeReport.load('~/Home/EmbedReport');
            }
        }
		window.Index = Menu.init();
		})(jQuery);
});

